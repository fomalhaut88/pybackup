# PyBackUp #

### Summary ###

PyBackUp is a crossplatform utility to store your data safely. It allows you to keep your files in different places (for instance, in different hard drives), to save modifying history, to restore files or directories you need. Also PyBackUp storage structure is very easy and it's possible to analyse it and to search for something there without PyBackUp.

Author: [Alexander Khlebuschev](http://en.fomalhaut.su)

### Downloading ###

[source link](http://downloads.fomalhaut.su/programs/pybackup.tar.gz)

### Installation for linux ###

* tar -xvzf pybackup.tar.gz
* sudo mv pybackup /opt/
* sudo ln -s /opt/pybackup/pybackup /usr/bin/pybackup

### How does PyBackUp work? ###

Firstly, you specify your source directory you need to store and also you specify another directory to backup. PyBackUP copies whole source directory to backup and creates a special file targets.pybackup, where it saves pairs source-target (source is your source directory and target is your backup directory). After your changes in source directory you may backup your source and PyBackUp will add to backup directory changed files only.

### Getting started ###

Suppose, you have got a directory /home/username/mydir and you need to store it in /media/drive2/backup. To do this via console:

* pybackup add source /home/username/mydir /media/drive2/backup

If your source added successfully, you can see your directory in sources list by command:

* pybackup show sources

When the source is added, in /media/drive2/backup new directory _home_username_mydir will be created. Consequenlty, you can add another sources by this way.

If you need to backup your directory:

* pybackup backup /home/username/mydir

To backup all sources:

* pybackup backupall

Backup process can take a few minutes. To show backups of specified source:

* pybackup show backups /home/username/mydir

To show history of a file /home/username/mydir/path/to/myfile.txt:

* pybackup history /home/username/mydir path/to/myfile.txt

To show file content in specified backup:

* pybackup content /home/username/mydir 20141205_153140 path/to/myfile.txt

If you need to restore the file from specified backup:

* pybackup restore /home/username/mydir 20141205_153140 path/to/myfile.txt
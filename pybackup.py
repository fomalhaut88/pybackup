#! /usr/bin/env python

import sys

from models import Console, Terminal


if __name__ == "__main__":
	argv = list(sys.argv)
	del argv[0]

	if argv:
		console = Console()
		console.execute(argv)
	else:
		terminal = Terminal()
		terminal.run()
"""
Model PyBackUp is a set of handlers to provide backup.
"""

import sys
import os

from models import BackUpList, BackUpSource



class PyBackUp(object):
	def __init__(self):
		backuplist_location = self.__get_backuplist_location()
		self.backuplist = BackUpList(backuplist_location)


	def __get_backuplist_location(self):
		if sys.platform in ('linux', 'linux2'):
			dirname = os.path.join(
				os.path.expanduser('~'),
				'.config',
				'pybackup'
			)
			if not os.path.exists(dirname):
				os.mkdir(dirname)
			return os.path.join(dirname, 'targets.pybackup')

		elif sys.platform == 'win32':
			dirname = os.path.join(
				os.path.expanduser('~'),
				'AppData/Local',
				'pybackup'
			)
			if not os.path.exists(dirname):
				os.mkdir(dirname)
			return os.path.join(dirname, 'targets.pybackup')

		else:
			return None
		
		
	def exit(self):
		print "Bye!"
		
		
	def show_sources(self):
		print "Sources:"
		if self.backuplist.get_sources():
			print "%-50s%s" % ("=*= SOURCE =*=", "=*= TARGET =*=")
		else:
			print "Empty set."
		for source in self.backuplist.get_sources():
			print "%-50s%s" % (source, self.backuplist.get_target(source))
		print
			
			
	def add_source(self, source, target):
		source = os.path.normpath(source)
		target = os.path.normpath(target)
		
		print "Adding %s to %s..." % (source, target)
		
		if not os.path.isdir(source):
			print "Wrong source %s." % source
			return
		
		if not os.path.isdir(target):
			print "Wrong target %s." % target
			return
		
		if source in self.backuplist.get_sources():
			print "The source %s already exists." % source
			return
		
		self.backuplist.add(source, target)
		backupsource = BackUpSource(source, target)
		backupsource.create()
		
		print "Completed."
		
		
	def remove_source(self, source):
		source = os.path.normpath(source)
		
		print "Removing %s..." % source
		
		if source not in self.backuplist.get_sources():
			print "The source %s doesn't exist." % source
			return
		
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		backupsource.drop()
		self.backuplist.remove(source)
		
		print "Completed."
		
		
	def show_backups(self, source):
		source = os.path.normpath(source)
		if source not in self.backuplist.get_sources():
			print "Invalid source %s." % source
			return
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		
		if backupsource is not None:
			backup_list = backupsource.list()
			print "\n".join(backup_list)
		else:
			print "Source is not used."
			
			
	def backup(self, source):
		source = os.path.normpath(source)
		if source not in self.backuplist.get_sources():
			print "Invalid source %s." % source
			return
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		
		print "Backup %s..." % source
		if backupsource.backup():
			print "Completed."
		else:
			print "Nothing to backup."
			
			
	def backupall(self):
		for source in self.backuplist.get_sources():
			self.backup(source)
			
			
	def history(self, source, file_path):
		source = os.path.normpath(source)
		file_path = os.path.normpath(file_path)
		
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		backupsource.history(file_path)
		
		
	def content(self, source, backup, file_path):
		source = os.path.normpath(source)
		file_path = os.path.normpath(file_path)
		
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		backupsource.content(backup, file_path)
		
		
	def restore(self, source, backup, file_path):
		source = os.path.normpath(source)
		file_path = os.path.normpath(file_path)
		
		backupsource = BackUpSource(source, self.backuplist.get_target(source))
		backupsource.restore(backup, file_path)
"""
Model ConsoleArguments handles input console parameters.

Usage example:

argv = ['myaction', '3', '4']

def myaction_handler(x, y):
	z = (int(x)**2 + int(y)**2)**0.5
	print z

console_arguments = ConsoleArguments()
console_arguments.add({
	"title": "myaction",
	"params": ['x', 'y'],
	"action": myaction_handler
})
console_arguments.execute(argv)

"""

class ConsoleArguments(object):
	command_prefix = ""


	def __init__(self):
		self.args = []
	

	def add(self, arg):
		self.args.append({
			"title": arg["title"],
			"params": arg["params"],
			"action": arg["action"]
		})
		

	def execute(self, argv):
		if not argv or argv[0] in ["-h", "--help", "help"]:
			self.help()
			return
		
		for arg in self.args:
			if (" ".join(argv) + " ").startswith(arg["title"] + " "):
				if len(argv) < len(arg["params"]) + len(arg["title"].split(" ")):
					print "Invalid number of parameters."
					return
				params = argv[len(arg["title"].split(" ")):]
				arg["action"](*params)
				return
			
		print "Invalid command '%s'." % " ".join(argv)
		print
		self.help()


	def help(self):
		print "Valid commands:"
		for arg in self.args:
			print "\t",
			if self.command_prefix:
				print self.command_prefix,
			print arg["title"],
			for param in arg["params"]:
				print "[%s]" % param,
			print				
		print
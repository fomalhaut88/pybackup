"""
Terminal mode.
"""

import re
from models import ConsoleArguments, PyBackUp



class Terminal(object):
	prompt = ">"

	def __init__(self):
		self.__pybackup = PyBackUp()
		self.__console_arguments = ConsoleArguments()
		self.__console_arguments.add({
			"title": "show sources",
			"params": [],
			"action": self.__pybackup.show_sources
		})
		self.__console_arguments.add({
			"title": "add source",
			"params": ["source", "target"],
			"action": self.__pybackup.add_source
		})
		self.__console_arguments.add({
			"title": "remove source",
			"params": ["source"],
			"action": self.__pybackup.remove_source
		})
		self.__console_arguments.add({
			"title": "show backups",
			"params": ["source"],
			"action": self.__pybackup.show_backups
		})
		self.__console_arguments.add({
			"title": "backup",
			"params": ["source"],
			"action": self.__pybackup.backup
		})
		self.__console_arguments.add({
			"title": "backupall",
			"params": [],
			"action": self.__pybackup.backupall
		})
		self.__console_arguments.add({
			"title": "history",
			"params": ["source", "file_path"],
			"action": self.__pybackup.history
		})
		self.__console_arguments.add({
			"title": "content",
			"params": ["source", "backup", "file_path"],
			"action": self.__pybackup.content
		})
		self.__console_arguments.add({
			"title": "restore",
			"params": ["source", "backup", "file_path"],
			"action": self.__pybackup.restore
		})


	@staticmethod
	def __parse_query(query):
		return map(
			lambda arg: arg.replace('\\s', ' '),
			re.sub(r"\\(\w)", r"\1", query).replace('\\ ', '\\s').split(' ')
		)


	def run(self):
		while True:
			query = raw_input(Terminal.prompt + " ")
			if query == "":
				continue
			elif query.lower() in ("exit", "quit"):
				break
			else:
				argv = self.__parse_query(query)
				self.__console_arguments.execute(argv)
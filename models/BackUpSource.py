"""
Model BackUpSource does some actions with backup source, for example creation of backup, getting list of backups, 
getting history of specified file, getting content of the file in specified backup, restoring specified version of the file.
"""

import os
import shutil
import filecmp
from datetime import datetime

from models import BackUpList



class BackUpSource(object):
	added_file_name = ".added.pybackup"
	
	
	def __init__(self, source, target):
		self.source = source
		self.target = target
		self.target_dir = self.__get_target_dir()
		
		
	def __get_target_dir(self):
		return os.path.join(
			self.target,
			BackUpList.source_title(self.source)
		)
		
	
	def __new_backup_name(self):
		"""
			Example of backup name: 20141205_153150
		"""
		return datetime.now().strftime("%Y%m%d_%H%M%S")
	
	
	def __copy_files(self, backup_name):
		shutil.copytree(
			self.source,
			os.path.join(self.target_dir, backup_name)
		)
		
		
	def __rel_source_path(self, path):
		return os.path.relpath(self.source, path)
	
	
	def __rel_target_dir_path(self, path):
		return os.path.relpath(self.target_dir, path)
	
	
	def __copy_rel(self, frm, to, rel_path):
		"""
			It copies file or folder from 'frm/rel_path' to 'to/rel_path' creating nonexistent folders.
		"""
		items = rel_path.split(os.sep)
		path = ""
		for item in items:
			path = os.path.join(path, item)
			if os.path.isdir(os.path.join(frm, path)):
				if not os.path.exists(os.path.join(to, path)):
					os.mkdir(os.path.join(to, path))
			else:
				shutil.copy(
					os.path.join(frm, path),
					os.path.join(to, path)
				)
	
		
	def create(self):
		"""
			It creates first backup.
		"""
		os.mkdir(self.target_dir)
		self.__copy_files(self.__new_backup_name())
		
		
	def drop(self):
		"""
			It drops backups.
		"""
		if os.path.exists(self.target_dir):
			shutil.rmtree(self.target_dir)
		else:
			print "Warning: cannot remove directory '%s', not found." % self.target_dir
		
		
	def list(self):
		"""
			It returns list of backups.
		"""
		backup_list = filter(
			lambda e: os.path.isdir(os.path.join(self.target_dir, e)),
			os.listdir(self.target_dir)
		)
		backup_list.sort()
		return backup_list
	
	
	def backup(self):
		"""
			It creates new backup.
		"""
		backup_target = self.list()[-1]
		backup_dir = os.path.join(self.target_dir, backup_target)
		
		# getting all source files
		source_files = set(map(
			lambda path: os.path.relpath(
				path,
				self.source
			),
			self.tree_files(self.source)
		))
		
		# getting all target files
		target_files = set(map(
			lambda path: os.path.relpath(
				path,
				backup_dir
			),
			self.tree_files(backup_dir)
		))
		
		# finding added and removed files
		general_files = source_files & target_files
		added_files = source_files - general_files
		removed_files = target_files - general_files
		
		# finding changed files
		changed_files = set(
			filter(
				lambda rel_path: not os.path.isdir(
					os.path.join(self.source, rel_path)
				) and not self.equal_content(
					os.path.join(self.source, rel_path),
					os.path.join(backup_dir, rel_path)
				),
				general_files
			)
		)
		
		# if it's possible to make backup
		if changed_files or added_files or removed_files:
			backup_name = self.__new_backup_name()
			backup_dir = os.path.join(self.target_dir, backup_name)
			backup_last = os.path.join(self.target_dir, backup_target)
			
			os.rename(backup_last, backup_dir)
			os.mkdir(backup_last)
			
			# changed files
			for rel_path in changed_files:
				self.__copy_rel(backup_dir, backup_last, rel_path)
				os.remove(os.path.join(backup_dir, rel_path))
				shutil.copy(
					os.path.join(self.source, rel_path),
					os.path.join(backup_dir, rel_path)
				)
			
			# removed files
			for rel_path in removed_files:
				self.__copy_rel(backup_dir, backup_last, rel_path)
			for rel_path in removed_files:
				path = os.path.join(backup_dir, rel_path)
				if os.path.exists(path):
					if os.path.isdir(path):
						shutil.rmtree(path)
					else:
						os.remove(path)
			
			# added files	
			if added_files:
				with open(os.path.join(backup_last, self.added_file_name), 'w') as f:
					f.write("\n".join(added_files))
				for rel_path in added_files:
					self.__copy_rel(self.source, backup_dir, rel_path)
					
			return True
		
		else:
			return False
		
		
	def history(self, file_path):
		"""
			It shows backups when file was been modified.
		"""
		# searching file at least in one backup
		history_exists = False
		for backup in self.list():
			file_path_full = os.path.join(self.target_dir, backup, file_path)
			if os.path.exists(file_path_full):
				history_exists = True
				break
		
		# if found, print these backups names
		if history_exists:
			print "History of file '%s':" % os.path.join(self.source, file_path)
			
			print "backup\t\t\tcreated time\t\t\tsize"
			for backup in self.list():
				file_path_full = os.path.join(self.target_dir, backup, file_path)
				if os.path.exists(file_path_full):
					created_date = datetime.strptime(backup, "%Y%m%d_%H%M%S")
					print "%s\t\t%s\t\t%s bytes" % (
						backup,
						created_date.strftime("%Y.%m.%d %H:%M"),
						os.path.getsize(file_path_full)
					)
		
		else:
			print "There is no history of file '%s'." % os.path.join(self.source, file_path)
			
			
	def content(self, backup, file_path):
		"""
			It shows content of specified file in specified backup.
		"""
		file_path_full = os.path.join(self.target_dir, backup, file_path)
		if os.path.exists(file_path_full):
			with open(file_path_full, 'rb') as f:
				print f.read()
		else:
			print "There is no file '%s' in backup '%s'." % (os.path.join(self.source, file_path), backup)
			
			
	def restore(self, backup, file_path):
		"""
			It restores specified file from specified backup.
		"""
		date = datetime.strptime(backup, "%Y%m%d_%H%M%S")
		
		for backup in sorted(self.list(), reverse=True):
			if datetime.strptime(backup, "%Y%m%d_%H%M%S") <= date:
				if os.path.exists(os.path.join(self.target_dir, backup, file_path)):
					if os.path.isdir(os.path.join(self.target_dir, backup, file_path)):
						# if folder, it restores only found files if specified backup
						for item in self.tree_files(os.path.join(self.target_dir, backup, file_path)):
							self.__copy_rel(
								os.path.join(self.target_dir, backup),
								self.source,
								os.path.relpath(os.path.join(self.target_dir, backup), item)
							)
							print backup, os.path.relpath(os.path.join(self.target_dir, backup), item)
					else:
						# if file, if restores content
						self.__copy_rel(
							os.path.join(self.target_dir, backup),
							self.source,
							file_path
						)
						print backup, file_path
					break
		print "Completed."
	
	
	@staticmethod
	def equal_content(path1, path2):
		return filecmp.cmp(path1, path2)
	
	
	@staticmethod
	def tree_files(path):
		if os.path.isdir(path):
			for item in os.listdir(path):
				item_path = os.path.join(path, item)
				if os.path.isdir(item_path):
					yield item_path
					for e in BackUpSource.tree_files(item_path):
						yield e
				else:
					yield item_path
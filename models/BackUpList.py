"""
Model BackUpList manages list of bacluped directories stored in backuplist_location.
"""

import os



class BackUpList(object):
	def __init__(self, backuplist_location):
		self.location = backuplist_location
		self.__data = {}
		self.__load()
		

	def get_target(self, source):
		return self.__data[source]
	

	def get_sources(self):
		return self.__data.keys()
	

	def __save(self):
		# writing backups to file
		with open(self.location, 'w') as f:
			for (source, target) in self.__data.items():
				f.write("%s %s\n" % (source, target))
				

	def __load(self):
		# creating new backuplist location
		if not os.path.exists(self.location):
			with open(self.location, 'w'):
				pass
		# reading backuplist file and filling self.__data
		with open(self.location, 'r') as f:
			self.__data = dict(map(
				lambda line: line.strip().split(" "),
				f.readlines()
			))
			

	def add(self, source, target):
		if source not in self.__data.keys():
			self.__data[source] = target
			self.__save()
			return True
		else:
			return False
		

	def remove(self, source):
		if source in self.__data.keys():
			self.__data.pop(source)
			self.__save()
			return True
		else:
			return False
		

	@staticmethod
	def source_title(source):
		title = source.replace(os.sep, "_")
		title = title.replace(":", "__")
		return title
"""
Console mode.
"""

import os

from models import ConsoleArguments, PyBackUp



class Console(object):
	def __init__(self):
		pybackup = PyBackUp()
		
		self.__console_arguments = ConsoleArguments()
		self.__console_arguments.command_prefix = "python pybackup_console.py"
		
		self.__console_arguments.add({
			"title": "show sources",
			"params": [],
			"action": pybackup.show_sources
		})
		self.__console_arguments.add({
			"title": "add source",
			"params": ["source", "target"],
			"action": pybackup.add_source
		})
		self.__console_arguments.add({
			"title": "remove source",
			"params": ["source"],
			"action": pybackup.remove_source
		})
		self.__console_arguments.add({
			"title": "show backups",
			"params": ["source"],
			"action": pybackup.show_backups
		})
		self.__console_arguments.add({
			"title": "backup",
			"params": ["source"],
			"action": pybackup.backup
		})
		self.__console_arguments.add({
			"title": "backupall",
			"params": [],
			"action": pybackup.backupall
		})
		self.__console_arguments.add({
			"title": "history",
			"params": ["source", "file_path"],
			"action": pybackup.history
		})
		self.__console_arguments.add({
			"title": "content",
			"params": ["source", "backup", "file_path"],
			"action": pybackup.content
		})
		self.__console_arguments.add({
			"title": "restore",
			"params": ["source", "backup", "file_path"],
			"action": pybackup.restore
		})


	def execute(self, argv):
		self.__console_arguments.execute(argv)